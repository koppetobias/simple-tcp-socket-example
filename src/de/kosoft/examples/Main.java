package de.kosoft.examples;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        try {
            /*
            Erzeuge Server Socket für Port 23456
             */
            ServerSocket serverSocket = new ServerSocket(23456);

            System.out.println("Start accepting connections ...");

            /*
            Endlosschleife damit Programm nicht beendet
             */
            while(true){

                /*
                Warte auf eingehende Verbindungen
                 */
                Socket socket = serverSocket.accept();

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            BufferedInputStream bIn = new BufferedInputStream(socket.getInputStream());
                            byte[] receivedBytes = new byte[0];
                            int n=0;
                            while(n>-1) {
                                byte[] readBuffer = new byte[1];
                                n=bIn.read(readBuffer);
                                if (n>-1) {
                                    receivedBytes = Arrays.copyOf(receivedBytes, receivedBytes.length+n);
                                    System.arraycopy(readBuffer,0,receivedBytes, receivedBytes.length-1,n);
                                }
                            }

                            if(receivedBytes.length==0){
                                System.out.println("Got nothing.");
                            }

                            bIn.close();
                            socket.close();
                            System.out.println("Received (bytes)["+receivedBytes.length+"] : " + bytesToHex(receivedBytes));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };

                Thread t = new Thread(r);
                t.start();
                
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String bytesToHex(byte[] num) {
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<num.length;i++) {
            char[] hexDigits = new char[2];
            hexDigits[0] = Character.forDigit((num[i] >> 4) & 0xF, 16);
            hexDigits[1] = Character.forDigit((num[i] & 0xF), 16);
            sb.append(hexDigits);
        }
        return sb.toString();
    }
}
